var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    email: {type: String, required: true}, // email юзера
    username: {type: String, required: true}, // обращение к юзеру
    questWords: {type: Array, required: true}, // слова для квеста
    questRightPhrases: {type: Array, required: true}, // правильные (победные) фразы
    expirationDate: {type: Date, required: true} // время, когда квест будет удален.
});

module.exports = mongoose.model('userQuest', schema);