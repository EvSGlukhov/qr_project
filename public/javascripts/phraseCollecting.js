var countOfWordsGlobal;

function addWordForms() {

    document.getElementById("word_1").style.visibility = "visible";
    document.getElementById("doneButton").style.visibility = "visible";

    var countOfWords = document.getElementById('countOfWords').value;
    countOfWordsGlobal = countOfWords;

    for (var i = 1; i < countOfWords; i++) {
        var wordForm = document.getElementById('word_'+i);
        var cloneForm = wordForm.cloneNode(true);
        var nextId = i+1;
        cloneForm.setAttribute('id', 'word_'+nextId);
        //cloneForm.style.margin-top = "5px";
        cloneForm.childNodes[0].childNodes[0].innerHTML = nextId;
        document.getElementById('wordForms').appendChild(cloneForm);
        //addWordForms();
    }

}

function fullyCloneDivWithElementInside(divID, elementID, innerText) {

    var arrayOfElementComponents = elementID.split('_');
    var numberOfElement = arrayOfElementComponents[arrayOfElementComponents.length - 1];
    var nextNumberOfElement = parseInt(numberOfElement) + 1;
    var newElementIDArray = [arrayOfElementComponents[0], nextNumberOfElement];
    var newElementID = newElementIDArray.join('_');
    
    var currentDIVElement = document.getElementById(elementID);
    var cloneForm = currentDIVElement.cloneNode(true);
    cloneForm.setAttribute('id', newElementID);
    //cloneForm.style.margin-top = "5px";
    cloneForm.text = innerText;
    document.getElementById(divID).appendChild(cloneForm);
    //addWordForms();

}

function getWordsInArray() {
    var countOfWords = countOfWordsGlobal;
    var words = [];

        for (var i = 1; i <= countOfWords; i++) {
        var wordForm = document.getElementById('word_'+i);
        var wordValue = wordForm.childNodes[0].childNodes[1].value;
        wordValue = wordValue.replace(/^\s+|\s+$/g, "");
        words.push(wordValue);
        }

    return words;
}

function getRightPhrasesArray(numberOfRightPhraseFields) {
    var countOfPhrases = numberOfRightPhraseFields;
    var phrases = [];

        for (var i = 1; i <= countOfPhrases; i++) {
        var baseID = 'inputPhraseField_';
        var currentPhraseID = baseID+i;
        var innerText = document.getElementById(currentPhraseID).value;
        innerText = innerText.replace(/^\s+|\s+$/g, "");
        phrases.push(innerText);
        }

    return phrases;
}
